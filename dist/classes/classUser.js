"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var UserAccount = /** @class */ (function () {
    function UserAccount(name, age) {
        this.name = name;
        this.age = age;
    }
    UserAccount.prototype.logDetails = function () {
        console.log("The player is " + this.name + " is " + this.age + " years old.");
    };
    UserAccount.prototype.getAge = function () {
        return this.age;
    };
    Object.defineProperty(UserAccount.prototype, "getName", {
        get: function () {
            return this.name;
        },
        enumerable: false,
        configurable: true
    });
    return UserAccount;
}());
var robert = new UserAccount('Robert', 70);
robert.logDetails();
/* -------------------------------------------- */
/*  Extends class - methods getters and setters
/* -------------------------------------------- */
var CharAccount = /** @class */ (function (_super) {
    __extends(CharAccount, _super);
    function CharAccount(name, age, nickname, level, skin) {
        var _this = _super.call(this, name, age) || this;
        _this.nickname = nickname;
        _this.level = level;
        _this.skin = skin;
        return _this;
    }
    CharAccount.prototype.getSkin = function () {
        console.log("This skin " + this.skin);
    };
    Object.defineProperty(CharAccount.prototype, "setLevel", {
        set: function (level) {
            if (level) {
                this.level = level;
            }
        },
        enumerable: false,
        configurable: true
    });
    return CharAccount;
}(UserAccount));
var jhon = new CharAccount('Jhon', 43, 'json', 19, 'blue');
console.log(jhon);
jhon.logDetails(); /* herança */
// jhon.nickname = "_robert" /* error, nickname is private of external objects */
console.log(jhon);
console.log(jhon.age);
// jhon.age = 1; /* Cannot assign to 'age' because it is a read-only property */
console.log(robert.getName); /* get é uma propriedade e não um função */
jhon.setLevel = 20;
console.log(jhon);
/* -------------------------------------------- */
/*  Abstract Class
/* -------------------------------------------- */
/* são usadas como classe modelo, mas não se pode criar objetos a partir dela */
var UserPlay = /** @class */ (function () {
    function UserPlay(name) {
        this.name = name;
    }
    return UserPlay;
}());
var UserGame = /** @class */ (function (_super) {
    __extends(UserGame, _super);
    function UserGame(name) {
        var _this = _super.call(this, name) || this;
        _this.name = name;
        return _this;
    }
    return UserGame;
}(UserPlay));
// const ford = new UserPlay('ford'); /* Cannot create an instance of an abstract class. */
