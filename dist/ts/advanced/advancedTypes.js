"use strict";
var account = {
    id: 1345,
    name: "Robert Ford",
};
var charAccount = {
    nickname: "_ford",
    level: 20
};
var playerInfo = {
    id: 54,
    name: "Robert",
    nickname: '_ford'
};
console.log(playerInfo); // { id: 54, name: 'Robert', nickname: '_ford' }
